@extends('layouts.app')

@section('content')
  <form>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-md-10 offset-md-1">
          <div class="card">
            <div class="card-header text-center">
              <strong>Gestionar Taquilla</strong>
            </div>
            <div class="card-body">
             
                <div class="form-group row">
                  <label class="col-md-3 col-form-label">Tipo de Vuelo</label>
                  <div class="col-md-9 col-form-label">
                    <div class="form-check form-check-inline mr-1">
                      <input class="form-check-input" type="radio" id="idayvuelta" value="idayvuelta" name="inline-radios">
                      <label class="form-check-label" for="idayvuelta"> Ida y Vuelta </label>
                    </div>
                    <div class="form-check form-check-inline mr-1">
                      <input class="form-check-input" type="radio" id="soloida" value="soloida" name="inline-radios">
                      <label class="form-check-label" for="soloida"> Solo Ida </label>
                    </div>
                    <div class="form-check form-check-inline mr-1">
                      <input class="form-check-input" type="radio" id="multidestino" value="multidestino" name="inline-radios">
                      <label class="form-check-label" for="multidestino"> Multi-destino </label>
                    </div>
                  </div>
                  <label class="col-md-2 col-form-label">Desde</label>
                  <div class="col-md-4 col-form-label">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fa fa-map-marker"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control datepicker" name="date">
                    </div>
                   
                  </div>
                   <label class="col-md-2 col-form-label">Hasta</label>
                  <div class="col-md-4 col-form-label">
                     <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fa fa-map-marker"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control">

                    </div>
                  </div>
                   <div class="col-md-4 col-form-label" id="sandbox-container">
                      <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fa fa-calendar"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-4 col-form-label" id="sandbox-container">
                      <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fa fa-calendar"></i>
                        </span>
                      </div>
                      <input type="text" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-4 col-form-label" >
                      <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fa fa-users"></i>
                        </span>
                      </div>
                     <select class="custom-select">
                      <option selected>Seleccione su Clase</option>
                      <option value="1">Economica</option>
                      <option value="2">Economica Plus</option>
                      <option value="3">Negocios</option>
                      <option value="4">Primera Clase</option>
                    </select>
                    </div>
                  </div>
                <label class="col-md-2 col-form-label">Adultos</label>
                <div class="col-md-2 col-form-label" >
                <div class="input-group ">
                  <span class="input-group-btn">
                    <button type="button" class="quantity-left-minus-adult btn btn-sm btn-danger btn-number"  data-type="minus" data-field="">
                      <span class="fa fa-minus"></span>
                    </button>
                  </span>
                  <input type="text" id="adult" name="adult" class="form-control form-control-sm input-number" value="1" min="1" max="9" style="text-align: center;">
                    <span class="input-group-btn">
                      <button type="button" class="quantity-right-plus-adult btn btn-sm btn-success btn-number" data-type="plus" data-field="">
                        <span class="fa fa-plus"></span>
                      </button>
                    </span>
                  </div>
                </div>
                <label class="col-md-2 col-form-label">Adolescente</label>
                <div class="col-md-2 col-form-label" >
                <div class="input-group ">
                  <span class="input-group-btn">
                    <button type="button" class="quantity-left-minus-child btn btn-sm btn-danger btn-number"  data-type="minus" data-field="" >
                      <span class="fa fa-minus"></span>
                    </button>
                  </span>
                  <input type="text" id="child" name="child" class="form-control form-control-sm input-number" value="0" min="1" max="100" style="text-align: center;">
                    <span class="input-group-btn">
                      <button type="button" class="quantity-right-plus-child btn btn-sm btn-success btn-number" data-type="plus" data-field="">
                        <span class="fa fa-plus"></span>
                      </button>
                    </span>
                  </div>
                </div>
                <label class="col-md-2 col-form-label text">Niño</label>
                <div class="col-md-2 col-form-label" >
                <div class="input-group ">
                  <span class="input-group-btn">
                    <button type="button" class="quantity-left-minus-infant btn btn-sm btn-danger btn-number"  data-type="minus" data-field="">
                      <span class="fa fa-minus"></span>
                    </button>
                  </span>
                  <input type="text" id="infant" name="infant" class="form-control form-control-sm input-number" value="0" min="1" max="9" style="text-align: center;">
                    <span class="input-group-btn">
                      <button type="button" class="quantity-right-plus-infant btn btn-sm  btn-success btn-number" data-type="plus" data-field="">
                        <span class="fa fa-plus"></span>
                      </button>
                    </span>
                  </div>
                </div>
               
                   </div>  
             
            </div>
            <div class="card-footer text-center">
              <button type="submit" class="btn btn-sm btn-primary">
                <i class="fa fa-dot-circle-o"> Aceptar</i>
              </button>
              <button type="reset " class="btn btn-sm btn-danger">
                <i class="fa fa-ban"> Cancelar</i>
              </button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</form>
<script type="text/javascript">
$('#sandbox-container input').datepicker({
    language: "es",
    autoclose: true,
    todayHighlight: true
});
</script>
@stop


